package dev.mbkaue.arvorebinaria;

/**
 *
 * @author mbkau
 */
public class NoA {
    int valor;
    NoA esq, dir;

    public NoA(int valor) {
        this.valor = valor;
        this.esq = null;
        this.dir = null;
    }
}